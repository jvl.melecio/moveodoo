echo Preparando directorios en VPS
ssh root@70.35.199.160 rm -rf odoo
ssh root@70.35.199.160 rm -rf postgresql
ssh root@70.35.199.160 mkdir odoo
ssh root@70.35.199.160 mkdir odoo/server
ssh root@70.35.199.160 mkdir odoo/addons
ssh root@70.35.199.160 mkdir postgresql
ssh root@70.35.199.160 mkdir postgresql/data
echo Exportando de contenedor a VPS
ssh root@70.35.199.160 docker cp prod-odoo:/var/lib/odoo/. odoo/server/.
ssh root@70.35.199.160 docker cp prod-odoo:/mnt/extra-addons/. odoo/addons/.
ssh root@70.35.199.160 docker cp prod-postgresql:/var/lib/postgresql/data/. postgresql/data/.
echo Deteniendo contenedores en esta PC
docker stop odoo
docker stop postgresql
echo Eliminando contenedores en esta PC
docker rm odoo
docker rm postgresql
echo Creando estructura de directorios en esta PC
mkdir C:\DockerVolumes
echo Importando datos desde el VPS
scp -r root@70.35.199.160:/root/odoo C:\DockerVolumes\odoo
scp -r root@70.35.199.160:/root/postgresql C:\DockerVolumes\postgresql
echo Importando docke-compose
scp -r root@70.35.199.160:/root/docker-compose.yml C:\DockerVolumes\docker-compose.yml
echo Creando contenedpres e iniciando servicios
docker-compose -f C:\DockerVolumes\docker-compose.yml up -d
pause
